<?php
/**
 * Podstawowa konfiguracja WordPressa.
 *
 * Ten plik zawiera konfiguracje: ustawień MySQL-a, prefiksu tabel
 * w bazie danych, tajnych kluczy, używanej lokalizacji WordPressa
 * i ABSPATH. Więćej informacji znajduje się na stronie
 * {@link http://codex.wordpress.org/Editing_wp-config.php Editing
 * wp-config.php} Kodeksu. Ustawienia MySQL-a możesz zdobyć
 * od administratora Twojego serwera.
 *
 * Ten plik jest używany przez skrypt automatycznie tworzący plik
 * wp-config.php podczas instalacji. Nie musisz korzystać z tego
 * skryptu, możesz po prostu skopiować ten plik, nazwać go
 * "wp-config.php" i wprowadzić do niego odpowiednie wartości.
 *
 * @package WordPress
 */

// ** Ustawienia MySQL-a - możesz uzyskać je od administratora Twojego serwera ** //
/** Nazwa bazy danych, której używać ma WordPress */
define('DB_NAME', 'baza21611_wp');

/** Nazwa użytkownika bazy danych MySQL */
define('DB_USER', 'admin21611_wp');

/** Hasło użytkownika bazy danych MySQL */
define('DB_PASSWORD', '6Py#DYQDh1');

/** Nazwa hosta serwera MySQL */
define('DB_HOST', '21611.m.tld.pl');

/** Kodowanie bazy danych używane do stworzenia tabel w bazie danych. */
define('DB_CHARSET', 'utf8');

/** Typ porównań w bazie danych. Nie zmieniaj tego ustawienia, jeśli masz jakieś wątpliwości. */
define('DB_COLLATE', '');

/**#@+
 * Unikatowe klucze uwierzytelniania i sole.
 *
 * Zmień każdy klucz tak, aby był inną, unikatową frazą!
 * Możesz wygenerować klucze przy pomocy {@link https://api.wordpress.org/secret-key/1.1/salt/ serwisu generującego tajne klucze witryny WordPress.org}
 * Klucze te mogą zostać zmienione w dowolnej chwili, aby uczynić nieważnymi wszelkie istniejące ciasteczka. Uczynienie tego zmusi wszystkich użytkowników do ponownego zalogowania się.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         '=& Z6FR-};1NJh|Vy9,]j4yaxDFNim6Q[39J@l*U+{Neoq|NgX#]rDzdc.&6*`++');
define('SECURE_AUTH_KEY',  '*u5Fa#1FDD+utxB[u _`>~C*,`;0Hk-3uls<0?nM&} TqqKWL3k~5?)NphcM;[hO');
define('LOGGED_IN_KEY',    'p)nK#R_k BlV{xChj),q~v>-b8=>| pax^+:DaPbRg!o1K0bs` v;,H+2P-^gH@4');
define('NONCE_KEY',        'K`5!B9$}@B?GOeY6~a<+>4]yx.uJ-P<% #;A_q]f ^.^2=K+]>*!Fec+#Zl f+YZ');
define('AUTH_SALT',        '-$y7;7&;K7&F%a{1#`Gyrj_&8}1,v6YJVPTd~MAe5wI-jR$4=[3SgUzTWjuX4 J1');
define('SECURE_AUTH_SALT', 'e9- s~+U,)$hrv_;0vY.B#^HsT.~BUIs0>w#bA|2QNPeRr6(?X+G^U};TP|xzV2i');
define('LOGGED_IN_SALT',   'A%,|NG>6s-F1jJ0Ez~RhY5H)F7fd;xK#=#q+c_buuS*DC-U0MQT$h+KL>FGtA]Cw');
define('NONCE_SALT',       '^NkAqO^N/f*BhK;_}ANIOt.Hg{W4E8rJ@O>BZB$dl@&e;oa,!].*v(I|Rm*)ya|*');

/**#@-*/

/**
 * Prefiks tabel WordPressa w bazie danych.
 *
 * Możesz posiadać kilka instalacji WordPressa w jednej bazie danych,
 * jeżeli nadasz każdej z nich unikalny prefiks.
 * Tylko cyfry, litery i znaki podkreślenia, proszę!
 */
$table_prefix  = 'wp_';

/**
 * Kod lokalizacji WordPressa, domyślnie: angielska.
 *
 * Zmień to ustawienie, aby włączyć tłumaczenie WordPressa.
 * Odpowiedni plik MO z tłumaczeniem na wybrany język musi
 * zostać zainstalowany do katalogu wp-content/languages.
 * Na przykład: zainstaluj plik de_DE.mo do katalogu
 * wp-content/languages i ustaw WPLANG na 'de_DE', aby aktywować
 * obsługę języka niemieckiego.
 */
define('WPLANG', 'pl_PL');

/**
 * Dla programistów: tryb debugowania WordPressa.
 *
 * Zmień wartość tej stałej na true, aby włączyć wyświetlanie ostrzeżeń
 * podczas modyfikowania kodu WordPressa.
 * Wielce zalecane jest, aby twórcy wtyczek oraz motywów używali
 * WP_DEBUG w miejscach pracy nad nimi.
 */
define('WP_DEBUG', false);

/* To wszystko, zakończ edycję w tym miejscu! Miłego blogowania! */

/** Absolutna ścieżka do katalogu WordPressa. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Ustawia zmienne WordPressa i dołączane pliki. */
require_once(ABSPATH . 'wp-settings.php');
