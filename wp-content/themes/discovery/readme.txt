Discovery is a responsive, highly customizable WordPress theme. Full instructions can be found at: http://www.templateexpress.com/Discovery-theme-instructions/

Fonts
-------------------------------------------
Genericons
vector icons embedded in a webfont designed to be clean and simple 
keeping with a generic aesthetic.
More info at http://genericons.com/

Open Sans
This Font Software is licensed under the Apache License v2.00
More info available with at http://www.apache.org/licenses/

Icons
--------------------------------------------
All icon images are by Template Express and are free to use in all commercial projects.

License
--------------------------------------------
License: GNU General Public License v2.0
License URI: http://www.gnu.org/licenses/gpl-2.0.html


