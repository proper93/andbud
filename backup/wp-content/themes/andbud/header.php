<!doctype html>
<html>
	<head>
            <title><?php echo bloginfo('title'); ?> || <?php echo bloginfo('description'); ?></title>
            
            <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
            <meta charset="utf-8">
            <?php wp_head(); ?>
        </head>
        <body>
            <header>
                <div class="container">
                    <div class="row">
                        <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3">
                            <h1 class="logo">
                                <a href="<?php echo bloginfo('url'); ?>">
                                    <?php echo bloginfo('title') ?> || <?php echo bloginfo('description'); ?>
                                </a>
                            </h1>
                        </div>
                        <div class="col-lg-9 col-md-9 col-sm-9 col-xs-9">
                            <div class="row">
                                <div class="col-lg-12">
                                    <a href="#" class="btn btn-default">
                                        sklep internetowy
                                    </a>
                                    <p class="tel">
                                        <small>tel.</small>
                                        <strong>012 656 04 88</strong>
                                    </p>
                                </div>
                            </div>
                            <div class="row">
                                <?php wp_nav_menu(array( 'location' => 'header-menu', 'menu_class' => 'main-menu', 'container' => '' )) ?>
                            </div>
                        </div>
                    </div>
                </div>
            </header>