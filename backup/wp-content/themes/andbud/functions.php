<?php 

function andbud_theme() {
    
    //google font 
    wp_register_style('andbud-font', 'http://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700|Lato:400,300,700&subset=latin,latin-ext');
    wp_enqueue_style('andbud-font');
    
    //bootstrap
    wp_register_style('bootstrap-theme', get_template_directory_uri() . '/lib/bootstrap-theme.min.css');
    wp_register_style('bootstrap-css', get_template_directory_uri() . '/lib/bootstrap.min.css');
    wp_enqueue_style('bootstrap-theme');
    wp_enqueue_style('bootstrap-css');
    
    //główny plik ze stylami 
    wp_register_style('main_style', get_stylesheet_uri(), array(), null);
    wp_enqueue_style('main_style');
    
    //jquery
    wp_register_script('jquery-1.11.1', get_template_directory_uri() . '/lib/jquery.js');
    wp_enqueue_script( 'jquery-lib', get_stylesheet_directory_uri() . '/lib/jquery.js', array( 'jquery', 'jquery-1.11.1' ) );
    
    //boostrap js
    wp_register_script('jquery-bootstrap', get_template_directory_uri() . '/lib/bootstrap.min.js');
    wp_enqueue_script('jquery-bootstrap');
    
}
add_action( 'wp_enqueue_scripts', 'andbud_theme' );

add_action( 'after_setup_theme' , 'register_menu' );

function register_menu() {
    
    register_nav_menu( 'header-menu', 'Główne menu' );
    
}


?>